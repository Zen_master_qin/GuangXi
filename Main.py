#!/usr/bin/evn python
# -*- coding:utf-8 -*-

import DataProcessing
import os
import sys
import logging
from pylab import mpl
from datetime import datetime, timedelta
from apscheduler.schedulers.blocking import BlockingScheduler

reload(sys)
sys.setdefaultencoding('utf8')

mpl.rcParams['font.sans-serif'] = ['STSong']  # Set default font
mpl.rcParams['axes.unicode_minus'] = False  # Solving the problem with display symbolic.
mpl.rcParams['axes.linewidth'] = 0.3

dataDir = "D:\\FigPlot\\Indata\\MICAPS\\"
inputDir = "Z:\\world_nwp\\ENVIRONMENT\\CUACE\\"
outputDir = "D:\\Tomcat\\webapps\\esgx\\sys\\mai_plts\\fcst\\"


""" Set logging """
logging.basicConfig(level=logging.INFO,
                    filename='./log.txt',
                    filemode='a',
                    format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s')


def GetTimeInfo():
    # Read last time in txt file, and calculate the datetime we need this time.
    with open(outputDir + 'tt.txt', 'r+') as f:
        last_time = f.readlines()[0]
        tar_time = (datetime.strptime(last_time, '%Y%m%d%H') + timedelta(hours=4)).strftime('%Y%m%d%H')
        latest_time = (datetime.strptime(last_time, '%Y%m%d%H') + timedelta(hours=12)).strftime('%Y%m%d%H')
        return tar_time, latest_time


def WriteNewTime(latest_time):
    with open(outputDir + 'tt.txt', 'w+') as f:
        f.write(latest_time)


def monitor_date(tt):
    date_now = datetime.now()
    last_t = datetime.strptime(tt[0], '%Y%m%d%H')
    delta_t = (date_now - last_t).seconds
    return delta_t


def patch_data(date_p):
    tt = (datetime.strptime(date_p, '%Y%m%d%H') + timedelta(hours=12)).strftime('%Y%m%d%H')
    str_tar = 'ENVIRONMENT-{}'.format(date_p)
    folder_name = 'NWP_HAZE2MICAPS{}'.format(date_p)
    DataProcessing.TarFile(str_tar, tt)
    DataProcessing.Routing(folder_name, tt)


def AddJob():
    # Read last time in txt file, and calculate the datetime we need this time.
    TimeInfo = GetTimeInfo()

    # Unpack files.
    str_tar = "ENVIRONMENT-%s" % (TimeInfo[0])
    print str_tar
    folder_name = "NWP_HAZE2MICAPS%s" % (TimeInfo[0])

    # unpack files
    tar = DataProcessing.TarFile(str_tar, TimeInfo[1])
    if tar == '2':
        DataProcessing.Routing(folder_name, TimeInfo[1])
        WriteNewTime(TimeInfo[1])
    else:
        skip_time = monitor_date(TimeInfo)
        if skip_time > 3600*20:
            logging.info('Missing data, will skip')
            WriteNewTime(TimeInfo[1])


if __name__ == "__main__":
    sched = BlockingScheduler()
    sched.add_job(AddJob, trigger='cron', minute='*/60', hour='*', misfire_grace_time=180)
    sched.start()


